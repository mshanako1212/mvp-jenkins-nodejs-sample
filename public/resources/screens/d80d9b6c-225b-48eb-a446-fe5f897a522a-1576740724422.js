jQuery("#simulation")
  .on("pageload", ".s-d80d9b6c-225b-48eb-a446-fe5f897a522a .pageload", function(event, data) {
    var jEvent, jFirer, cases;
    if(data === undefined) { data = event; }
    jEvent = jimEvent(event);
    jFirer = jEvent.getEventFirer();
    if(jFirer.is("#s-d80d9b6c-225b-48eb-a446-fe5f897a522a")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/f85875fb-5e23-4dde-ad82-c034f3989f63",
                    "transition": {
                      "type": "fade",
                      "duration": 20000
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    }
  });